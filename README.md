# trucs_et_bidules

Des trucs divers et variés à propos de SPIP

## Règles de nommage des tags pour les plugins
en attendant la mise à jour de la doc de création des plugins de:
https://plugins.spip.net/distribuer-une-contribution.html
et
https://www.spip.net/fr_article3458.html
```
{{{Règles concernant plus spécifiquement les plugins}}}
7. une fois la fusion réalisée les mainteneurs du plugins devraient:

    - si besoin/utile incrémenter le n° de version du plugin dans le fichier paquet.xml, de la forme {{version="x.y.z"}}

    - créer un tag pour diffuser cette nouvelle version dans le canal de mise à jour des plugins SPIP. La nomenclature conseillée pour les tags est: {{v1.2.3}}
```
